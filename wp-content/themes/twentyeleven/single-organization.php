<?php
/**
 * Template Name:  Template Organisation
 *
 * Description: A Page Template that adds a sidebar to pages.
 *
 */

if (!is_user_logged_in())
{
 wp_redirect(home_url());
}
else
{
  require_once(WP_PLUGIN_DIR . '/aui/invite.php') ;
  $invitation = new Invite();
  if (isset($_POST['submit'])) {
        if(isset($_POST['email'])){
          $organizationName = str_replace(' ', '_', $_POST['organisationName']);

            $invitation->setEmail($_POST['email']);
            $invitation->setOrganisation($organizationName);
            $invitation->setRole($_POST['role']);
            $invitation->setOrganisationID($_POST['organisationID']);
            Invite::hashUrl($invitation);
            $known = Invite::checkIfInviteExists($invitation);
            if(empty($known)){
              if(isset($_POST['message'])){
                $typedText = $_POST['message'];
                $typedText = nl2br($typedText);
                $typedText = stripslashes($typedText);
                $invitation->setMessage($typedText);
              }else{
                  $invitation->setMessage("");
              }
              Invite::insertNewInvite($invitation);
            }





        }else{
          _e("Vul een geldig email adres in" , 'aui');
        }


}

?>

 <html>
    <body>
       <head>
          <?php
 get_header();
?>
       </head>
       <section>

       </section>
       <aside>
         <?php
 if (have_posts()):
  while (have_posts()):
   the_post();
?>
 <?php
   echo the_content();
?>
<?php
  endwhile;
 endif;
?>

<form method="POST" id="usrform">
<table>



        Email: <input type="email" name="email"/><br>
        Bericht: <textarea name="message" form="usrform"></textarea>
       </aside>

       <input id="button" style="float:left;" class="btn-3" type='submit' name='submit'/>
       <input type="hidden" name="role" value="0011"/>
       <input type="hidden" name="organisationID" value="10"/>
       <input type="hidden" name="organisationName" value="testorganisatie"/>


       <footer>
          <?php get_footer(); ?>
       </footer>
    </body>
 </html>
<?php
}
