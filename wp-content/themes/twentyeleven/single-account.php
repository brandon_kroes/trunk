<?php
/**
 * Template Name:  Template UserPage
 *
 * Description: A Page Template that adds a sidebar to pages.
 *
 */

 if ( !is_user_logged_in() ) {
 wp_redirect( home_url() );
 }else{
 ?>

 <html>
    <body>
       <head>
          <?php get_header(); ?>
       </head>
       <section>
         <?php include( 'menu/usermenu.php') ?>
       </section>
       <aside>
         <?php if (have_posts()) : while (have_posts()) : the_post();?>
 <?php  echo the_content(); ?>
<?php endwhile; endif; ?>
       </aside>
       <footer>
          <?php get_footer(); ?>
       </footer>
    </body>
 </html>
<?php
}
?>
