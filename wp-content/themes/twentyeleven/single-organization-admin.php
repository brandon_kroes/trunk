<?php
/**
 * Template Name:  Template Admin
 * Description: Een pagina die uitnodigen van een Admin en het verwijderen van een Admin toestaat.
 *
 */


    require_once(WP_PLUGIN_DIR . '/aui/classes/invite.php');
    require_once(WP_PLUGIN_DIR . '/aui/classes/organisation.php');

    global $wpdb;
    $table_name     = $wpdb->prefix . 'usersInOrganisation';
    $user_id        = get_current_user_id();

    //Acquiring the organisation ID where the current user is administrator, this can only be one.
    $organisationID = $wpdb->get_var($wpdb->prepare('SELECT organisation_id FROM ' . $table_name . ' where role=3 and user_id= %s', $user_id));

    //Checking if the user is an administrator of a certain organi
    if(isset($organisationID)){
    $organisation = new Organisation();
    $organisation->setOrganisationID($organisationID);

    //Checking the amount of administrators for the datatable and stroing them in a local variable
    $records = $organisation->organisationAdministrators();
    $recordAmount = count($records);


    //Check if the organisationID is available, because otherwise the rest of invitation system won't work
    if ($organisationID != null) {
        //if the delete settings are called this will be used, it deletes a user from an organisation based on the userID and the OrganisationID
        if (isset($_POST['delete'])) {

          $delete = $_POST['delete'];
          foreach( $delete as $k ) {
            $organisation->removeMember($k);
          }

        }
            if (isset($_POST['email'])) {
                $invitation = new Invite();
                $invitation->setEmail($_POST['email']);
                $invitation->setRole(3);
                $invitation->setOrganisationID($organisationID);
                $invitation->organisationName();
                $admin = Organisation::isAdminEmail($invitation->getEmail());
                if($admin == true){
                  Invite::hashUrl($invitation);
                  $known = Organisation::isInOrganisation($organisationID, $invitation->getEmail(),  $invitation->getRole());
                  var_dump($known);
                  var_dump($invitation->getEmail());
                  if ($known === NULL) {
                      if (isset($_POST['message'])) {
                          $typedText = $_POST['message'];
                          $typedText = nl2br($typedText);
                          $typedText = stripslashes($typedText);
                          $invitation->setMessage($typedText);
                      }
                      Invite::insertNewInvite($invitation);
                      Invite::sendEmailInvite($invitation);

                  } else {
                      _e("Deze gebruiker bevindt zich al in de organisatie!", 'aui');
                  }
                }else{
                    _e("Neem contact op met de servicedesk over het promoten van deze specifieke gebruiker", 'aui');
                }
            }
      }
    }else{
      //In this scenario a user does not have Administrator access
       wp_redirect( home_url() );
    }

?>



 <html>
    <body>
          <head>
          <?php get_header(); ?>

          </head>
      <section>

      </section>
       <aside>
         <?php
        if (have_posts()):
            while (have_posts()):
                the_post();
?>
 <?php echo the_content();

            endwhile;
        endif;
?>

<form method="POST" id="usrform">
        Email: <input type="email" name="email"/><br>
        Bericht: <textarea name="message" form="usrform"></textarea>

       <input style="float:left;" type='submit'/>

</form>

  <hr>

<form method="POST" id="usrform">

<table>
        <thead>
            <tr>
                <th>Naam</th>
                <th>Verwijderen</th>
            </tr>
        </thead>

        <tbody>
          <br>
<?php
          for($i = 0; $i < $recordAmount; $i++){
                $user_id = $records[$i]->user_id;
                $user_data = get_userdata($user_id);
                echo "<tr> <td>";
                echo $user_data->display_name;
                echo " </td>";

                echo "<td><input type='checkbox' name='delete[]' value='". $user_id. "'/>";
                echo " </td> </tr>";
          } ?>
            </tbody>
    </table>
     <input type='submit' />
</form>

<footer>
          <?php get_footer(); ?>
       </footer>
    </body>
 </html>
