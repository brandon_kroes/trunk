<?php
/**
* Template Name: Template Userdata
* Description: Een pagina waarin de gebruiker zijn gebruikersinformatie kan vinden *
*/


if ( !is_user_logged_in() ) {
wp_redirect( home_url() );
}else{

  $user_id = get_current_user_id();
?>

<html>
   <body>
      <head>
         <?php get_header(); ?>
      </head>
      <section>
        <?php include( 'menu/usermenu.php') ?>
      </section>
      <aside>

                <form method="POST" id="locationinformation">
                        <?php _e( 'Email' , 'aui');?>
                        <input type="email" name="email" value="<?php echo implode(get_user_meta($user_id, 'email')); ?>" />
                        <br>
                        <?php _e( 'Street' , 'aui');?>
                        <input type="text" name="street" value="<?php echo implode(get_user_meta($user_id, 'street')); ?>" />
                        <br>
                        <?php _e( 'Number' , 'aui');?>
                        <input type="number" name="number" value="<?php echo implode(get_user_meta($user_id, 'number')); ?>" />
                        <br>
                        <?php _e( 'Zipcode' , 'aui');?>
                        <input type="text" name="zipcode" value="<?php echo implode(get_user_meta($user_id, 'zipcode')); ?>" />
                        <br>
                        <?php _e( 'Place' , 'aui');?>
                        <input type="text" name="place" value="<?php echo implode(get_user_meta($user_id, 'place')); ?>" />
                        <br>
                        <?php _e( 'Description' , 'aui')?>
                        <input type="text" name="description" value="<?php echo implode(get_user_meta($user_id, 'description')); ?>" />
                        <br>
                        <?php _e( 'Skypename' , 'aui')?>
                        <input type="text" name="skypename" value="<?php echo implode(get_user_meta($user_id, 'skypename')); ?>" />
                        <br>



                          <div id="billingDiv"  style="display:none;" class="answer_list" >

                        <hr>
                        <?php _e( 'Street' , 'aui');?>
                        <input type="text" name="street_billing" value="<?php echo implode(get_user_meta($user_id, 'street_billing')); ?>" />
                        <br>
                        <?php _e( 'Number' , 'aui');?>
                        <input type="number" name="number_billing" value="<?php echo implode(get_user_meta($user_id, 'number_billing')); ?>" />
                        <br>
                        <?php _e( 'Zipcode' , 'aui');?>
                        <input type="text" name="zipcode_billing" value="<?php echo implode(get_user_meta($user_id, 'zipcode_billing')); ?>" />
                        <br>
                        <?php _e( 'Place' , 'aui');?>
                        <input type="text" name="place_billing" value="<?php echo implode(get_user_meta($user_id, 'place_billing')); ?>" //>
                        <br>
                         </div>
                        <input id="button" type='submit' />

                </form>


                  <input type="button" name="answer" value="<?php _e('Factuur Adres ' , 'aui'); ?>" onclick="showDiv()" />


        </aside>
          <footer>
                <?php get_footer(); ?>
          </footer>
</body>

</html>
<?php } ?>
