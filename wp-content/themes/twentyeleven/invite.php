<?php
global $wpdb;
class Invite
{
    //Variables
    private $organisation;
    private $organisationID;
    private $email;
    private $hashurl;
    private $role;
    private $message;
    private $userID;
    //getters
    public function getUserID()
    {
        return $this->userID;
    }
    public function getOrganisation()
    {
        return $this->organisation;
    }
    public function getOrganisationID()
    {
        return $this->organisationID;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getHashUrl()
    {
        return $this->hashurl;
    }
    public function getRole()
    {
        return $this->role;
    }
    public function getMessage()
    {
        return $this->message;
    }
    
    //setters
    public function setUserID($value){
        $this->userID = $value;
    }
    public function setOrganisationID($value)
    {
        $this->organisationID = $value;
    }
    public function setOrganisation($value)
    {
        $this->organisation = $value;
    }
    public function setEmail($value)
    {
        $this->email = $value;
    }
    public function setHashUrl($value)
    {
        $this->hashurl = $value;
    }
    public function setRole ($value)
    {
      $this->role =$value;
    }
    public function setMessage ($value)
    {
      $this->message = $value;
    }
    //Constuctor
    public function __construct()
    {
    }

    //Methods
    public static function hashUrl($obj)
    {
        //initiating random values
        $time =  time();
        $randomnumber = mt_rand();
        //filling the hashurl values
        $hashvalues = '';
        $hashvalues.= $obj->email;
        $hashvalues.=$randomnumber;
        $hashvalues.=$time;
        //Adding organisation prefix
        $hashurl = $obj->organisation;
        $hashurl .=  '-';
        $hashurl .= sha1($hashvalues);
        //return is the organisation_name followed by the hashurl consisting out of a time, number and email combination
        $obj->setHashUrl($hashurl);
    }
    public function insertNewUserIn ($obj)
    {
        $user_id = get_current_user_id();
        //Insert into UsersInOrganisation
        //Contains User_ID, Organisation_id, Role
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $wpdb->insert($table_name, array(
          'organisation_id' => $organisation_id,
          'user_id' => $user_id,
          'Role' => $role
        ), array(
          '%s',
          '%s',
          '%d')
      );
    }
    public static function insertNewInvite ($obj)
    {
      global $wpdb;
        //function that creates a new invite record based on an email, organisation and a hash created by hashUrl
        $table_name = $wpdb->prefix . 'invites';
        var_dump($obj->role);
        $wpdb->query($wpdb->prepare("INSERT INTO $table_name (`organisation_id`, `email`, `hash`, `role`) VALUES ('%d', '%s', '%s', '%d')",
                    array($obj->organisationID,
                          $obj->email,
                          $obj->hashurl,
                          //b'' is needed to insert it as a bit value
                          "b'.$obj->role.'")));
        echo $wpdb->show_errors();
    }
    public static function checkIfInviteExists($obj)
    {
      global $wpdb;
      $table_name = $wpdb->prefix . 'invites';
      $known = $wpdb->get_var('SELECT * FROM ' . $table_name . ' where email="'. $obj->email .'" and organisation_id  =' . $obj->organisationID . ' and role = ' . $obj->role);
      if($known == 0){
        return true;
      }else{
        return false;
      }

    }

    public function removeFromOrganization(){
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $wpdb->delete($table_name, array('user_id' => $this->id, 'organisation_id' => $this->organizationID ));
    }

    public function upgradeUserRole(){
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $wpdb->update(
            $table_name,
            array(
                'year' =>  'value2'  // integer (number)
            ),
            array( 'organiz' => 1 ),
            array(
                '%s',   // value1
            ),
            array( '%d' )
        );

    }
    public static function sendEmailInvite($obj)
    {
        //function that sends an email with a hashUrl, custom message, an organisation name to a specific email adres
        $content = __('Hier is uw Uitnodiging zin');
        //Here a link needs to be added to redirect the user to origin domain name, currentl
        $content .= ' ' . get_site_url() .'/'. $obj->hashurl . ' ';
        if(isset($obj->message)){
              $content.= $obj->message;
        }
        $subject = __('Uitnodiging Organisatie');

        //Mail is being send with wp-bettermail to make it look better
        wp_mail( $obj->email, $subject, $content);
    }
    public static function inviteAccepted ($hashUrl)
    {
        //Update function to set the DEFAULT false of a user to a true
        $wpdb->update(
            	'table',
            	array(
                'accepted' => TRUE
            	),
            	array( 'hash' => $hashUrl ),
            	array(
            		'%s',
            	),
            	array( '%s' )
            );

            $wpdb->

    }
}
