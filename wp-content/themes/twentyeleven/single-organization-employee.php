<?php
/**
 * Template Name:  Template Medewerker
 *
 * Description: Een pagina die uitnodigen van een medewerker, het verwijderen van een medewerker en het promoten van een medewerker toestaat
 *
 */

if (!is_user_logged_in()) {
    wp_redirect(home_url());
} else {
    require_once(WP_PLUGIN_DIR . '/aui/invite.php');
    global $wpdb;
    $table_name     = $wpdb->prefix . 'usersInOrganisation';
    $user_id        = get_current_user_id();
    $organizationID = $wpdb->get_var($wpdb->prepare('SELECT organisation_id FROM ' . $table_name . ' where role=15 and user_id= %s', $user_id));
    if ($organizationID != null) {
        if (isset($_POST['delete'])) {
          $invitation = new Invite();
          $invitation->setOrganisationID($organizationID);
          $invitation->setUserID($_POST['userID']);
          $invitation->removeFromOrganization();
          echo "Gebruiker verwijdert";
        }
        if(isset($_POST['promotion'])){
          $invitation = new Invite();
          $invitation->setMessage(__('standaard email tekst'));
          $invitation->setOrganisationID($organizationID);
          $invitation->setUserID($_POST['userID']);
        }
        if (isset($_POST['submit'])) {
            $invitation = new Invite();
            if (isset($_POST['email'])) {
                $organizationName = str_replace(' ', '_', "organisatie");
                $invitation->setEmail($_POST['email']);
                $invitation->setOrganisation($organizationName);
                $invitation->setRole($_POST['role']);
                $invitation->setOrganisationID($organizationID);
                Invite::hashUrl($invitation);
                $known = Invite::checkIfInviteExists($invitation);
                if ($known == true) {
                    if (isset($_POST['message'])) {
                        $typedText = $_POST['message'];
                        $typedText = nl2br($typedText);
                        $typedText = stripslashes($typedText);
                        $invitation->setMessage($typedText);
                    }
                    Invite::insertNewInvite($invitation);
                    Invite::sendEmailInvite($invitation);
                    echo "succes";
                } else {
                    echo "De gebruiker heeft al een uitnodiging gekregen, controlleer de spam folder!";
                }

            } else {
                _e("Vul een geldig email adres in", 'aui');
            }


        }

?>

 <html>
    <body>
       <head>
          <?php get_header(); ?>
       </head>
       <section>

       </section>
       <aside>
         <?php
        if (have_posts()):
            while (have_posts()):
                the_post();
?>
 <?php echo the_content();

            endwhile;
        endif;
?>

<form method="POST" id="usrform">
        Email: <input type="email" name="email"/><br>
        Bericht: <textarea name="message" form="usrform"></textarea>

       <input id="button" style="float:left;" class="btn-3" type='submit' name='submit'/>
       <input type="hidden" name="role" value="1"/>
</form>

</aside>
       <footer>
          <?php get_footer(); ?>
       </footer>
    </body>
 </html>
<?php
    } else {
        wp_redirect(home_url());
    }
}
