<?php
/*
 *Plugin Name: Added User Interface
 *Plugin URI: http://www.inesta.nl
 *Description: Plugin designed to give the updated user interactions in the SimpelGo Wordpress instance
 *Version: 0.1
 *Author: Brandon Kroes
 *Author URI: http://www.inesta.nl
 *License: GPL
*/

//On plugin start -> 1. Database tables are created, Organisation post type is initialised and shortcodes are being created

require_once(WP_PLUGIN_DIR . '/aui/classes/database.php');
require_once(WP_PLUGIN_DIR . '/aui/post/organisation-post-type.php');
require_once(WP_PLUGIN_DIR . '/aui/post/organisation-post-fields.php');
require_once(WP_PLUGIN_DIR . '/aui/classes/users.php');
require_once(WP_PLUGIN_DIR . '/aui/classes/organisation.php');


//For testing purposes there is a deactivationPlugin function that deletes the database tables created
//It is called using register_deactivation_hook
function deactivationPlugin() {

        global $wpdb;
        $table_name = $wpdb->prefix . 'invites';
        $wpdb->query("DROP TABLE IF EXISTS $table_name");
        $table_name = $wpdb->prefix . 'usersInOrganisation';
	      $wpdb->query("DROP TABLE IF EXISTS $table_name");

}


register_deactivation_hook( __FILE__, 'deactivationPlugin' );

add_shortcode( 'accountPage', 'accountPage' );
add_shortcode( 'userdatapage', 'userDataPage' );
