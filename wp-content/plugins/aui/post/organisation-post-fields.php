<?php

require(WP_PLUGIN_DIR . '/aui/classes/organisation.php');

function aui_addMemberMetaBox(){

  add_meta_box(
    //Name to distinguish from others
    'organisationMetaBox',
    //The title that is given to the metabox in the backend
    __('Organisatie Overzicht', 'aui'),
    //Function that will be executed for the HTML and or PHP
    'organisationMetabox_callback',
    //The Post type
    'organisatie',
    //Location: Normal or Side
    'normal',
    //Priority compared to other metaboxes, High puts it above all except Editor.
    'high'

  );

}

function organisationMetabox_callback()
{
    global $post;

    //Function to get back all members
    $members = Organisation::organisationMembers($post->ID);

    // var_dump($members);
    ?>
    <html>
    <style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
    <table style='float:center'>

        <tr>
            <th>Naam</th>
            <th>Rol</th>
            <tr>

                <?php for($i = 0; $i < count($members); $i++)
                { ?>

                <tr>
                    <td>
                        <?php echo $members[$i]->display_name ?></td>
                    <td><?php Organisation::roleConvert($members[$i]->role);

                    }  ?></td>
                </tr>

              

            </table>
        </html>
        <?php
}

add_action('add_meta_boxes', 'aui_addMemberMetaBox');

?>
