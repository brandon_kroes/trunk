<?php

global $wpdb;

class OrganizationInit
{

    function __construct()
    {
        add_action('init', array(
            $this,
            'addOrganizationPostType'
        ));
    }

    function addOrganizationPostType()
    {
        register_post_type('organisatie', array(
            'labels' => array(
                'name' => _x('Organisaties', 'post type general name', 'sc_aui'),
                'singular_name' => _x('Organisatie', 'post type singular name', 'sc_aui'),
                'menu_name' => _x('Organisaties', 'admin menu', 'sc_aui'),
                'name_admin_bar' => _x('Organisatie', 'add new on admin bar', 'sc_aui'),
                'add_new' => _x('Organisatie toevoegen', 'Organisatie', 'sc_aui'),
                'add_new_item' => __('Nieuwe Organisatie', 'sc_aui'),
                'new_item' => __('Organisatie toevoegen', 'sc_aui'),
                'edit_item' => __('Organisatie Wijzigen', 'sc_aui'),
                'view_item' => __('Alle Organisatie', 'sc_aui'),
                'all_items' => __('Alle Organisaties', 'sc_aui'),
                'search_items' => __('Zoek een Organisatie', 'sc_aui'),
                'parent_item_colon' => __('Organisaties:', 'sc_aui'),
                'not_found' => __('Geen Organisatie gevonden.', 'sc_aui'),
                'not_found_in_trash' => __('Geen Organisatie gevonden in de prullenbak', 'sc_aui')
            ),

            // Frontend
            'has_archive' => false,
            'public' => false,
            'publicly_queryable' => false,

            // Admin
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-businessman',
            'menu_position' => 10,
            'query_var' => true,
            'show_in_menu' => true,
            'show_ui' => true,
            'supports' => array(
                'title',
                'author',
                'thumbnail'


            )
        ));
    }

}

$organizationInit = new OrganizationInit;
