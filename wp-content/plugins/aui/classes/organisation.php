<?php

//Functie om alle deelnemers van een organisatie op te vragen
//Functie om alle medewerkers van een organisatie op te vragen
//Functie om alle administrators van een organisatie op te vragen


class Organisation {

    private $organisation;
    private $organisationID;

    //getters
    public function getOrganisation()
    {
        return $this->organisation;
    }
    public function getOrganisationID()
    {
        return $this->organisationID;
    }

    //setters
    public function setOrganisationID($value)
    {
        $this->organisationID = $value;
    }
    public function setOrganisation($value)
    {
        $this->organisation = $value;
    }

    //Constuctor
    public function __construct()
    {

    }

    public static function isAdminID($ID){
        global $wpdb;
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $organisationID = $wpdb->get_var($wpdb->prepare('SELECT organisation_id FROM ' . $table_name . ' where role=3 and user_id= %s', $user_id));
        return $organisationID;
    }
    public static function isEmployeeID($ID){
        global $wpdb;
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $organisationID = $wpdb->get_var($wpdb->prepare('SELECT organisation_id FROM ' . $table_name . ' where role=2 and user_id= %s', $user_id));
        return $organisationID;
    }

    public static function isParticipantID($ID){
        global $wpdb;
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $organisationID = $wpdb->get_var($wpdb->prepare('SELECT organisation_id FROM ' . $table_name . ' where role=1 and user_id= %s', $user_id));
        return $organisationID;
    }

    public static function isInOrganisation($organisationID, $email, $role)
    {

      global $wpdb;
      $table_name = $wpdb->prefix . 'users';
      $IDUser = $wpdb->get_var("SELECT ID from $table_name WHERE user_email = '$email' " );

      var_dump($IDUser);
      if($IDUser != 0){
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $known = $wpdb->get_var(" SELECT COUNT(*) from $table_name WHERE user_id= $IDUser  and organisation_id= $organisationID and role = b'". decbin($role) ."'");
        return $known;






      }
    }

    public static function isAdminEmail($email)
    {

      global $wpdb;
      $table_name = $wpdb->prefix . 'users';
      $IDUser = $wpdb->get_var("SELECT ID from $table_name WHERE user_email = '. $email .'" );

      if($IDUser != 0){
        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $known = $wpdb->get_var("SELECT COUNT(*) from $table_name WHERE user_id= $IDUser  and role = 3" );
        return $known;
        if($known == 0){
          return true;
        }else{
          return false;
        }

      }else{
        return true;
      }

    }

    //function to grab all display_names and roles from members of a certain organisation based on a ID
    public static function organisationMembers($organisationID)
    {
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $table_name_user = $wpdb->prefix . 'users';
      $members = $wpdb->get_results("SELECT $table_name.role, $table_name_user.display_name FROM wp_usersInOrganisation, $table_name_user where $table_name.organisation_id = 13 and $table_name.user_id = $table_name_user.ID");

      return $members;
    }

    public static function roleConvert($role)
    {
      switch($role) {
        case 1 : return _e("Deelnemer", 'aui');
        break;
        case 2: return _e("Medewerker", 'aui');
        break;
        case 3: return _e("Beheerder", 'aui');
        break;
      }
    }

    public function organisationParticipants()
    {
      //Participant role = 1
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $records = $wpdb->get_results( "SELECT * FROM $table_name WHERE role='1'" );
      return $records;
    }

    public function organisationEmployees()
    {
      //Employee role = 2
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $records = $wpdb->get_results( "SELECT * FROM $table_name WHERE role='2'" );
      return $records;
    }

    public function organisationAdministrators()
    {
      //Admin role = 3
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $records = $wpdb->get_results( "SELECT * FROM $table_name WHERE role='3'" );
      return $records;
    }

    public function removeMember($id)
    {
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $wpdb->delete($table_name, array('user_id' => $id, 'organisation_id' => $this->organisationID ));
    }

    public function promoteUser($id)
    {
      global $wpdb;
      $table_name = $wpdb->prefix . 'usersInOrganisation';
      $wpdb->update(
            $table_name,
            array(
                'user_id' =>  $id,  // integer (number)
                'role' => 2,
                'organisationID' => $this->organisationID
            ),
            array( 'role' => 3 ),
            array(
                '%d',   // value1
                '%d',
                '%d'
            ),
            array( '%d' )
        );

    }


}



?>
