<?php
global $wpdb;
class Invite
{
    //Variables
    private $organisation;
    private $organisationID;
    private $email;
    private $hashurl;
    private $role;
    private $message;
    private $userID;

    //getters
    public function getUserID()
    {
        return $this->userID;
    }
    public function getOrganisation()
    {
        return $this->organisation;
    }
    public function getOrganisationID()
    {
        return $this->organisationID;
    }
    public function getEmail()
    {
        return $this->email;
    }
    public function getHashUrl()
    {
        return $this->hashurl;
    }
    public function getRole()
    {
        return $this->role;
    }
    public function getMessage()
    {
        return $this->message;
    }

    //setters
    public function setUserID($value)
    {
        $this->userID = $value;
    }
    public function setOrganisationID($value)
    {
        $this->organisationID = $value;
    }
    public function setOrganisation($value)
    {
        $this->organisation = $value;
    }
    public function setEmail($value)
    {
        $this->email = $value;
    }
    public function setHashUrl($value)
    {
        $this->hashurl = $value;
    }
    public function setRole ($value)
    {
      $this->role =$value;
    }
    public function setMessage ($value)
    {
      $this->message = $value;
    }
    //Constuctor
    public function __construct()
    {

    }

    //Methods
    public static function hashUrl($obj)
    {
        //initiating random values
        $time =  time();
        $randomnumber = mt_rand();
        //filling the hashurl values
        $hashvalues = '';
        $hashvalues.= $obj->email;
        $hashvalues.=$randomnumber;
        $hashvalues.=$time;
        //Adding organisation prefix
        $hashurl = $obj->organisation;
        $hashurl .=  '-';
        $hashurl .= sha1($hashvalues);
        //return is the organisation_name followed by the hashurl consisting out of a time, number and email combination
        $obj->setHashUrl($hashurl);
    }

    public function organisationName()
    {

      global $wpdb;

      $table_name = $wpdb->prefix . 'posts';
      $organisationName = $wpdb->get_var('SELECT post_title FROM ' . $table_name . ' where ID='. $this->organisationID);
      $this->organisation = $organisationName;
      return $organisationName;
    }


    public static function insertNewInvite ($obj)
    {

        global $wpdb;
        //function that creates a new invite record based on an email, organisation and a hash created by hashUrl
        $table_name = $wpdb->prefix . 'invites';
        $sql = "INSERT INTO $table_name
          (`organisation_id`,`email`,`hash`,`role`)
             values ($obj->organisationID, '$obj->email', '$obj->hashurl', b'". decbin($obj->role) . "')";

          $wpdb->query($sql);

    }

    public static function checkIfInviteExists($obj)
    {
      //function that checks if the user already has an invite existing for the current user
      global $wpdb;
      $table_name = $wpdb->prefix . 'invites';
      $known = $wpdb->get_var('SELECT * FROM ' . $table_name . ' where email="'. $obj->email .'" and organisation_id  =' . $obj->organisationID . ' and role = ' . $obj->role);

      return $known;

    }






    public static function sendEmailInvite($obj)
    {
        //function that sends an email with a hashUrl, custom message, an organisation name to a specific email adres.
        //function will only operate if all values if submited so it's imperative to use it last.
        $content = __('Hier is uw Uitnodiging zin');

        //Here a link needs to be added to redirect the user to origin domain name
        $content .= ' ' . get_site_url() .'/'. $obj->hashurl . ' ';
        if(isset($obj->message)){
              $content.= $obj->message;
        }
        $subject = __('Uitnodiging Organisatie');

        //Mail is being send with wp-bettermail to make it look better
        wp_mail( $obj->email, $subject, $content);
    }


    //INACTIVE FUNCTIONS


    // public function insertNewUserInOrganisation ($obj)
    // {
    //     $user_id = get_current_user_id();
    //     //Insert into UsersInOrganisation
    //     //Contains User_ID, Organisation_id, Role
    //     $table_name = $wpdb->prefix . 'usersInOrganisation';
    //     $wpdb->insert($table_name, array(
    //       'organisation_id' => $organisation_id,
    //       'user_id' => $user_id,
    //       'Role' => $role
    //     ), array(
    //       '%s',
    //       '%s',
    //       '%d')
    //   );
    // }


    // public static function inviteAccepted ($hashUrl)
    // {
    //     //Update function to set the DEFAULT false of a user to a true
    //     $wpdb->update(
    //         	'table',
    //         	array(
    //             'accepted' => TRUE
    //         	),
    //         	array( 'hash' => $hashUrl ),
    //         	array(
    //         		'%s',
    //         	),
    //         	array( '%s' )
    //         );
    //
    //
    // }
}
