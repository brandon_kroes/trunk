<?php

global $wpdb;
class databaseInit
{

    function __construct()
    {
        add_action('init', array(
            $this,
            'creationDatabase'
        ));
    }

    function creationDatabase()
    {
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $table_name = $wpdb->prefix . 'usersInOrganisation';
        $sql        = " CREATE TABLE $table_name (
      `user_id` INT NOT NULL,
      `organisation_id` INT NOT NULL,
      `role` BIT(4) NOT NULL)
      ENGINE = InnoDB;";

        dbDelta($sql);

        $table_name = $wpdb->prefix . 'invites';
        $sql        = "CREATE TABLE $table_name (
      `organisation_id` INT NOT NULL,
      `email` VARCHAR(45) NOT NULL,
      `creation_date` DATETIME NOT NULL,
      `hash` VARCHAR(45) NOT NULL,
      `accepted` TINYINT(1) NOT NULL DEFAULT FALSE,
      `role` BIT(4) NOT NULL,
      PRIMARY KEY (`hash`));";

        dbDelta($sql);

    }
}

$databaseInit = new databaseInit;
