<?php

global $wpdb;

class User
{

  //Variables
  private $userID;


  //Getters
  public function getUserID()
  {
      return $this->userID;
  }


  //Setters
  public function setUserID($value)
  {
      $this->userID = $value;
  }

  //Constuctor
  public function __construct()
  {

  }

  //Methods
  public function getOrganisations()
  {
    global $wpdb;
    $table_posts = $wpdb->prefix . 'posts';
    $table_organisation = $wpdb->prefix . 'usersInOrganisation';

    $organisations = $wpdb->get_results("select $table_posts.post_title, $table_organisation.role from $table_posts, $table_organisation where $table_organisation.user_id = 2 and  $table_organisation.organisation_id = $table_posts.ID ");

    return $organisations;
  }

}
